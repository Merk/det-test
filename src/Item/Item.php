<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 15.05.17 14:17
 */

namespace Capture\Item;


use Capture\Component;

class Item extends Component {


	public function initialize() {
		$this->setSource( 'capture_items' );
	}


	/**
	 * Adding new file item
	 *
	 * @param string $filename
	 *
	 * @return bool|int
	 */
	public function addItem(string $filename)
	{

		$data = [
			'filename' => $filename,
			'create_datetime' => date("Y-m-d H:i:s")
		];

		if ($this->save($data)) {
			return (int)$this->{'id'};
		}

		return false;

	}




}