<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 15.05.17 20:05
 */

namespace Capture;

use Phalcon\DiInterface;
use Phalcon\Di\InjectionAwareInterface;
use \Phalcon\Db\Adapter\Pdo\Mysql;

abstract class Component implements InjectionAwareInterface {


	private $_di;


	/**
	 * Component constructor.
	 *
	 * @param DiInterface $di
	 */
	public function __construct(DiInterface $di)
	{
		$this->setDi($di);
	}

	/**
	 * @param DiInterface $di
	 */
	public function setDi(DiInterface $di)
	{
		$this->_di = $di;
	}

	/**
	 * @return mixed
	 */
	public function getDi()
	{
		return $this->_di;
	}

	/**
	 * @return \Phalcon\Db\Adapter\Pdo\Mysql
	 */
	public function getDb() : Mysql
	{
		return $this->getDi()->getShared('db');
	}



}